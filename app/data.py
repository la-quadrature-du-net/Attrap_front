import click
import os
import shutil
import sys
import glob
import re
import datetime
import pathlib

from flask import abort
from flask import Blueprint
from flask import current_app
from flask import jsonify
from flask import request

from opensearchpy.exceptions import RequestError

from minio import Minio

from . import cache

bp = Blueprint('data', __name__)


@bp.cli.command('create-index')
def create_index():
    current_app.logger.info('Création de l\'index dans OpenSearch...')
    try:
        index_name = 'attrap-index'
        index_body = {
            'settings': {
                'index': {
                    'number_of_shards': 1,
                    'number_of_replicas': 1,
                    'highlight':{
                        'max_analyzed_offset': 10000000
                    }
                },
                'analysis': {
                    'analyzer': {
                        'attrap_analyzer': {
                            'tokenizer': 'standard',
                            'filter': [ 'attrap_stemmer', 'attrap_asciifolding', 'lowercase' ]
                        }
                    },
                    'filter': {
                        'attrap_stemmer': {
                            'type': 'stemmer',
                            'language': 'french'
                        },
                        'attrap_asciifolding': {
                            'type': 'asciifolding',
                            'preserve_original': True
                        }
                    }
                }
            },
            'mappings': {
                'properties': {
                    'content': {
                        'type': 'text',
                        'analyzer': 'attrap_analyzer'
                    },
                    'date': {
                        'type': 'date',
                        'format' : 'yyyy-MM-dd'
                    },
                    'first_seen_on': {
                        'type': 'date',
                        'format' : 'date_time_no_millis'
                    },
                    'pdf_creation_date': {
                        'type': 'date',
                        'format' : 'date_time_no_millis'
                    },
                    'pdf_modification_date': {
                        'type': 'date',
                        'format' : 'date_time_no_millis'
                    },
                    'administration': {
                        'type': 'text'
                    },
                    'timezone': {
                        'type': 'text'
                    }
                }
            }
        }
        response = current_app.opensearch.indices.create(index_name, body=index_body)
    except Exception as exc:
        current_app.logger.error(f'Impossible de créer l\'index: {exc}')
        sys.exit(1)

@bp.cli.command('delete-index')
def delete_index():
    try:
        index_name = 'attrap-index'
        response = current_app.opensearch.indices.delete(index_name)
    except Exception as exc:
        current_app.logger.error(f'Impossible de supprimer l\'index: {exc}')
        sys.exit(1)

@bp.cli.command('index-files')
@click.argument('administration', default=None, required=False)
@click.option('--force', default=False, is_flag=True, help="Indexe les fichiers même s'ils l'ont déjà été précédemment.")
def index_files_cli(administration, force):
    """Indexe les fichiers d'une administration, ou de toutes si ADMINISTRATION est vide."""
    index_files(administration, force)

def index_files(administration, force):
    available_administrations = os.listdir(f'{current_app.root_path}/../data/')
    for current_administration in available_administrations:
        if administration is None or administration == current_administration:
            current_app.logger.info(f'Mise à jour des données de {current_administration}')
            available_raa = glob.glob(os.path.join(f'{current_app.root_path}/../data/{current_administration}/raa/', '*.json'))
            skipped = 0
            indexed = 0
            for raa in available_raa:
                current_app.logger.debug(f'Indexation de {raa}')
                filename_root = re.sub('\\.json$', '', raa)
                if os.path.isfile(f'{filename_root}.ok') and not force:
                    current_app.logger.info(f'{raa} a déjà été indexé')
                    skipped = skipped + 1
                else:
                    json_content = current_app.json.load(open(raa))
                    document_content = open(f'{filename_root}.txt').read()
                    file_id = filename_root.split('/')[-1]

                    document = {
                        'name': json_content['name'],
                        'administration': current_administration,
                        'date': json_content['date'],
                        'url': json_content['url'],
                        'first_seen_on': json_content['first_seen_on'],
                        'pdf_creation_date': json_content['pdf_creation_date'],
                        'pdf_modification_date': json_content['pdf_modification_date'],
                        'timezone': json_content['timezone'],
                        'content': document_content
                    }

                    response = current_app.opensearch.index(
                        index = 'attrap-index',
                        body = document,
                        id = file_id,
                        params = {
                            'timeout': 120
                        }
                    )

                    f = open(f'{filename_root}.ok', 'a')
                    f.write('')
                    f.close()

                    indexed = indexed + 1

                    current_app.logger.info(response)
    current_app.logger.info('Nettoyage du cache...')
    cache.clear()
    current_app.logger.info('Mise à jour de la liste des administrations disponibles...')
    current_app.config['AVAILABLE_ADMINISTRATIONS'] = get_available_administrations(current_app)
    return {
        'skipped': skipped,
        'indexed': indexed
    }

@bp.cli.command('download-files')
@click.argument('administration', default=None, required=False)
@click.option('--no-fail', required=False, default=False, is_flag=True, help="Ne retourne pas de code d'erreur si le téléchargement a échoué.")
def download_files_cli(administration, no_fail):
    """Télécharge les fichiers de données à partir d'un bucket S3."""

    if s3_configured(current_app.config['S3']):
        if administration:
            download_files(administration, no_fail)
        else:
            for administration in current_app.config['ADMINISTRATIONS']:
                download_files(administration, no_fail)
    else:
        current_app.logger.error('Le bucket S3 n\'est pas configuré.')
        sys.exit(1)

def download_files(administration, no_fail):
    if s3_configured(current_app.config['S3']):
        current_app.logger.info(f'Téléchargement de {administration}')

        host = current_app.config['S3']['HOST']
        bucket = current_app.config['S3']['BUCKET']
        access_key = current_app.config['S3']['ACCESS_KEY']
        secret_key = current_app.config['S3']['SECRET_KEY']

        data_dir = f'{current_app.root_path}/../data/'
        zip_file = f'{data_dir}/{administration}.zip'

        pathlib.Path(data_dir).mkdir(parents=True, exist_ok=True)

        try:
            # On se connecte au bucket
            client = Minio(
                host,
                access_key=access_key,
                secret_key=secret_key,
            )
            response = client.get_object(bucket, f'{administration}.zip')

            # On enregistre les données vers le fichier
            file = open(zip_file, 'wb')
            shutil.copyfileobj(response, file)
            file.close()

            # On ferme la connexion
            response.close()
            response.release_conn()

            # On dézippe le fichier
            shutil.unpack_archive(zip_file, data_dir)

            # On supprime l'archive
            os.remove(zip_file)
        except Exception as exc:
            if no_fail:
                current_app.logger.warn(f'Impossible de télécharger les données de {administration}: {exc}')
            else:
                current_app.logger.error(f'Impossible de télécharger les données de {administration}: {exc}')
                sys.exit(1)
    else:
        current_app.logger.error('Le bucket S3 n\'est pas configuré.')

@bp.cli.command('list-administrations')
def list_administrations_cli():
    """Affiche la liste des administrations configurées et disponibles"""

    for administration in get_available_administrations(current_app):
        print(f"{administration} ({current_app.config['ADMINISTRATIONS'][administration]})")

def get_available_administrations(app):
    available_administrations = {}

    # Pour chaque administration, on vérifie qu'il y a au moins un RAA dans la base de données
    for administration in app.config['ADMINISTRATIONS']:
        query = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'match': {
                                'administration': administration,
                            }
                        }
                    ]
                }
            }
        }
        try:
            response = app.opensearch.count(
                body = query,
                index = 'attrap-index'
            )
            if response.get('count', 0) > 0:
                available_administrations = available_administrations | {administration: app.config['ADMINISTRATIONS'][administration]}
        except Exception:
            pass
    return available_administrations

@bp.route('/data/update-hook', methods=['GET'])
def receive_hook():
    secret_key = request.args.get('secret_key', '', type=str)
    administration = request.args.get('administration', '', type=str)

    if current_app.config['UPDATE_HOOK_SECRET_KEY'] is None or current_app.config['UPDATE_HOOK_SECRET_KEY'] == 'changeme':
        current_app.logger.info(f'Demande de mise à jour des données recue, mais le secret n\'a pas été configuré.')
        return abort(404)

    if secret_key != current_app.config['UPDATE_HOOK_SECRET_KEY']:
        return abort(403)

    if administration is None or administration == '' or not administration in current_app.config['ADMINISTRATIONS']:
        return abort(400)

    if not s3_configured(current_app.config['S3']):
        current_app.logger.error('Le bucket S3 n\'est pas configuré.')
        return abort(500)

    download_files(administration, False)
    indexed = index_files(administration, False)

    response = jsonify({
        'status': 'ok'
    } | indexed)

    return response

@bp.route('/data/update-hook-gitlab', methods=['POST'])
def receive_hook_gitlab():
    secret_key = request.headers.get('X-Gitlab-Token', '', type=str)

    if current_app.config['UPDATE_HOOK_SECRET_KEY'] is None or current_app.config['UPDATE_HOOK_SECRET_KEY'] == 'changeme':
        current_app.logger.info(f'Demande de mise à jour des données recue, mais le secret n\'a pas été configuré.')
        return abort(404)

    if secret_key != current_app.config['UPDATE_HOOK_SECRET_KEY']:
        return abort(403)

    if not s3_configured(current_app.config['S3']):
        current_app.logger.error('Le bucket S3 n\'est pas configuré.')
        return abort(500)

    try:
        hook = request.json
        build_status = hook.get('build_status')
        administration = hook.get('build_name').replace('test_', '')
        object_kind = hook.get('object_kind')
        build_stage = hook.get('build_stage')

    except Exception:
        return abort(400)

    if not object_kind or object_kind != 'build' or not build_status or build_stage != 'test':
        return jsonify({'status': 'ignored'})

    if build_status:
        if build_status == 'success':
            if administration is None or administration == '' or not administration in current_app.config['ADMINISTRATIONS']:
                return abort(400)

            download_files(administration, False)
            indexed = index_files(administration, False)

            response = jsonify({
                'status': 'ok'
            } | indexed)

            return response
        else:
            return jsonify({'status': 'ignored'})
    else:
        return abort(400)

def s3_configured(config_s3):
    return config_s3 is not None and config_s3 != '' and \
        config_s3['HOST'] is not None and config_s3['HOST'] != '' and \
        config_s3['BUCKET'] is not None and config_s3['BUCKET'] != '' and \
        config_s3['ACCESS_KEY'] is not None and config_s3['ACCESS_KEY'] != '' and \
        config_s3['SECRET_KEY'] is not None
