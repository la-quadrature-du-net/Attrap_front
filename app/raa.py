import datetime
import locale
import pytz

from flask import abort
from flask import Blueprint
from flask import current_app
from flask import jsonify
from flask import render_template
from flask import request

from . import cache
from . import search

bp = Blueprint("raa", __name__)


@bp.route('/raa/<raa_id>', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def raa(raa_id):
    query = request.args.get('s', '', type=str)
    administrations=search.get_selected_administrations(request.args.getlist('administration'))
    start_date = search.get_date(request.args.get('start_date', '', type=str))
    end_date = search.get_date(request.args.get('end_date', '', type=str))
    sort = search.get_sort(request.args.get('sort', '', type=str))
    size = request.args.get('size', '', type=str)
    page = request.args.get('page', '', type=str)

    result = get_raa_in_opensearch(raa_id, search=query, pre_tags='<mark>', post_tags='</mark>')
    if result:
        # On transforme les dates en quelque chose de lisible
        try:
            locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
        except Exception:
            pass

        tz = pytz.timezone(result['timezone'])

        result['date'] = datetime.datetime.strptime(result['date'], '%Y-%m-%d').strftime('%d %B %Y')
        if result['pdf_creation_date']:
            result['pdf_creation_date'] = datetime.datetime.fromisoformat(result['pdf_creation_date']).astimezone(tz).strftime('%d %B %Y à %H:%m:%S')
        if result['pdf_modification_date']:
            result['pdf_modification_date'] = datetime.datetime.fromisoformat(result['pdf_modification_date']).astimezone(tz).strftime('%d %B %Y à %H:%m:%S')
        if result['first_seen_on']:
            result['first_seen_on'] = datetime.datetime.fromisoformat(result['first_seen_on']).astimezone(tz).strftime('%d %B %Y à %H:%m:%S')

        return render_template(
            'raa.html',
            raa=result,
            search=query,
            page=page,
            size=size,
            administrations=administrations,
            start_date=start_date,
            end_date=end_date,
            sort=sort
        )
    else:
        return abort(404)

@bp.route('/api/v1/raa/<raa_id>', methods=['GET'])
@cache.cached(timeout=0)
def api_v1_raa(raa_id):
    result = get_raa_in_opensearch(raa_id)
    if result:
        return jsonify(result)
    else:
        return abort(404)

def get_raa_in_opensearch(raa_id=None, search=None, pre_tags='<em>', post_tags='<em>'):
    if raa_id is None:
        return None
    else:
        query = {
            'query': { 
                'bool': {
                    'filter': {
                        'term': {
                            '_id': raa_id
                        }
                    }
                }
            }
        }

        if search:
            query['highlight'] = {
                'pre_tags': pre_tags,
                'post_tags': post_tags,
                'fields': {
                    'content': {
                        'type': 'plain',
                        'highlight_query': {
                            'bool': {
                                'should': [
                                    {
                                        'query_string': {
                                            'query': f'{search}'
                                        }
                                    }
                                ],
                            }
                        }
                    }
                }
            }

        response = current_app.opensearch.search(
            body = query,
            index = 'attrap-index'
        )

        if response['hits']['total']['value'] == 0:
            return None
        else:
            if current_app.config['AVAILABLE_ADMINISTRATIONS'].get(response['hits']['hits'][0]['_source']['administration']):
                response['hits']['hits'][0]['_source']['administration_name'] = current_app.config['AVAILABLE_ADMINISTRATIONS'].get(response['hits']['hits'][0]['_source']['administration'])
            else:
                response['hits']['hits'][0]['_source']['administration_name'] = response['hits']['hits'][0]['_source']['administration']

            if response['hits']['hits'] != []:
                response['hits']['hits'][0]['_source']['content'] = response['hits']['hits'][0]['_source']['content'].strip()
                if search:
                    # On remplace le texte surligné dans le contenu du RAA
                    for string in response['hits']['hits'][0]['highlight']['content']:
                        original = string.replace(pre_tags, '').replace(post_tags, '')
                        response['hits']['hits'][0]['_source']['content'] = response['hits']['hits'][0]['_source']['content'].replace(original, string)
                return response['hits']['hits'][0]['_source'] | { 'id': response['hits']['hits'][0]['_id'] }
            else:
                return None
