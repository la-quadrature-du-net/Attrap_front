from flask import Blueprint
from flask import abort
from flask import current_app
from flask import redirect
from flask import url_for

from flask.logging import default_handler

import logging

bp = Blueprint("aliases", __name__)

logger = logging.getLogger(__name__)
logger.addHandler(default_handler)


@bp.route("/<alias>")
def alias(alias):
    if not current_app.config['ALIASES'].get(alias):
        abort(404)
    else:
        if isinstance(current_app.config['ALIASES'][alias], str):
            return redirect(url_for('search.search', s=current_app.config['ALIASES'][alias], _external=True))
        else:
            if not current_app.config['ALIASES'][alias]['search']:
                logger.error(f'L\'alias {alias} n\'est pas correctement configuré : valeur "search" manquante')
                abort(500)
            else:
                search = current_app.config['ALIASES'][alias]['search']

            if 'administration' in current_app.config['ALIASES'][alias] and current_app.config['ALIASES'][alias]['administration']:
                if isinstance(current_app.config['ALIASES'][alias]['administration'], str):
                    administration = current_app.config['ALIASES'][alias]['administration']
                elif isinstance(current_app.config['ALIASES'][alias]['administration'], list):
                    administration = ','.join(current_app.config['ALIASES'][alias]['administration'])
                else:
                    value_type = type(current_app.config['ALIASES'][alias]['administration'])
                    logger.error(f'L\'alias {alias} n\'est pas correctement configuré : la valeur "administration" est incorrecte ({value_type})')
                    abort(500)
            else:
                administration = None

            if 'sort' in current_app.config['ALIASES'][alias]:
                sort = current_app.config['ALIASES'][alias]['sort']
                if sort != 'relevance' and sort != 'desc' and sort != 'asc':
                    logger.error(f'L\'alias {alias} n\'est pas correctement configuré : la valeur "sort" est incorrecte ({sort})')
                    abort(500)
            else:
                sort = None

            return redirect(url_for('search.search', s=search, administration=administration, sort=sort, _external=True))
