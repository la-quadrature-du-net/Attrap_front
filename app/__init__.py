import logging
import os
import pathlib
import shutil
import sys
import yaml

from flask import Flask
from flask import jsonify
from flask import make_response
from flask import render_template
from flask import request
from flask.logging import default_handler
from flask_caching import Cache
from flask_assets import Environment, Bundle
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

from werkzeug.middleware.proxy_fix import ProxyFix
from werkzeug.datastructures import ResponseCacheControl

from opensearchpy import OpenSearch

cache = Cache()

logger = logging.getLogger(__name__)
logger.addHandler(default_handler)


def create_app():
    app = Flask(__name__)
    app.root_path = os.path.dirname(os.path.abspath(__file__))
    app.config.from_file(f"{app.root_path}/../config.default.yml", load=config_yaml_loader, text=False)
    app.config.from_file(f"{app.root_path}/../config.yml", load=config_yaml_loader, silent=True, text=False)

    # Si le mode debug est activé, on n'utilise pas de cache, sinon on utilise Redis/Valkey
    if app.config['DEBUG']:
        app.config['CACHE_TYPE'] = 'NullCache'
    else:
        app.config['CACHE_TYPE'] = 'RedisCache'
        app.config['CACHE_KEY_PREFIX'] = 'attrap_'
        app.config['CACHE_REDIS_HOST'] = app.config['REDIS_HOST']
        app.config['CACHE_REDIS_PORT'] = app.config['REDIS_PORT']
        app.config['CACHE_REDIS_PASSWORD'] = app.config['REDIS_PASSWORD']

    # Initialisation du cache
    cache.init_app(app)
    cache.clear()

    # Paramétrage du reverse proxy
    if app.config['DEBUG'] is False:
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1)

    # Paramétrage du rate-limit (en production, on utilise Redis, sinon la RAM)
    if app.config['DEBUG']:
        rate_limit_storage_string = 'memory://'
    else:
        redis_password = app.config['REDIS_PASSWORD']
        redis_host = app.config['REDIS_HOST']
        redis_port = app.config['REDIS_PORT']
        if not redis_password == '':
            redis_string = f'redis://{redis_password}@{redis_host}:{redis_port}'
        else:
            redis_string = f'redis://{redis_host}:{redis_port}'
        rate_limit_storage_string = redis_string

    limiter = Limiter(
        get_remote_address,
        app=app,
        application_limits=app.config['RATE_LIMITS'],
        storage_uri=rate_limit_storage_string,
        strategy='fixed-window-elastic-expiry',
        key_prefix='attrap_rate_limit_'
    )

    # On copie le CSS et JS de Bootstrap dans le dossier public
    os.makedirs(f'{app.root_path}/static/vendors/bootstrap', exist_ok=True)
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/css/bootstrap.min.css', f'{app.root_path}/static/vendors/bootstrap/bootstrap.min.css')
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/css/bootstrap.min.css.map', f'{app.root_path}/static/vendors/bootstrap/bootstrap.min.css.map')
    shutil.copy(f'{app.root_path}/../vendors/bootstrap/dist/js/bootstrap.bundle.min.js', f'{app.root_path}/static/vendors/bootstrap/bootstrap.bundle.min.js')

    # Bundle du CSS
    assets = Environment(app)
    css = Bundle('vendors/bootstrap/bootstrap.min.css', 'styles.css', filters='rcssmin', output='gen/css.css')
    assets.register('css_all', css)

    # Bundle du Javascript
    js = Bundle('vendors/bootstrap/bootstrap.bundle.min.js', 'app.js', filters='rjsmin', output='gen/js.js')
    assets.register('js_all', js)

    administrations = {}
    for administration in app.config['ADMINISTRATIONS']:
        administrations = administrations | administration
    app.config['ADMINISTRATIONS'] = administrations

    aliases = {}
    if app.config.get('ALIASES'):
        for alias in app.config['ALIASES']:
            aliases = aliases | alias
    app.config['ALIASES'] = aliases

    from . import aliases
    from . import data
    from . import index
    from . import raa
    from . import search

    app.register_blueprint(aliases.bp)
    app.register_blueprint(data.bp)
    app.register_blueprint(index.bp)
    app.register_blueprint(raa.bp)
    app.register_blueprint(search.bp)

    limiter.exempt(data.bp)

    app.add_url_rule("/", endpoint="index")

    if not app.config['OPENSEARCH_CA_CERTS'] == '':
        ca_certs = app.config['OPENSEARCH_CA_CERTS']
    else:
        ca_certs = None

    if not app.config['OPENSEARCH_CLIENT_CERT'] == '':
        client_cert = app.config['OPENSEARCH_CLIENT_CERT']
    else:
        client_cert = None

    if not app.config['OPENSEARCH_CLIENT_KEY'] == '':
        client_key = app.config['OPENSEARCH_CLIENT_KEY']
    else:
        client_key = None

    app.opensearch = OpenSearch(
        hosts = [{
            'host': app.config['OPENSEARCH_HOST'],
            'port': app.config['OPENSEARCH_PORT']
        }],
        http_compress = app.config['OPENSEARCH_HTTP_COMPRESS'],
        http_auth = (app.config['OPENSEARCH_USERNAME'], app.config['OPENSEARCH_PASSWORD']),
        use_ssl = app.config['OPENSEARCH_USE_SSL'],
        verify_certs = app.config['OPENSEARCH_VERIFY_CERTS'],
        ssl_assert_hostname = app.config['OPENSEARCH_SSL_ASSERT_HOSTNAME'],
        ssl_show_warn = app.config['OPENSEARCH_SSL_SHOW_WARN'],
        ca_certs = ca_certs,
        client_cert = client_cert,
        client_key = client_key,
    )

    @app.after_request
    def after_request(response):
        try:
            extension = pathlib.Path(request.path).suffix
            response.cache_control.no_cache = None
            if extension == '.css' or extension == '.js' or extension == '.svg':
                response.cache_control.max_age = 15778463
                response.cache_control.s_maxage = 15778463
                response.cache_control.public = True
                response.cache_control.immutable = True
            else:
                response.cache_control.no_cache = True
        except Exception as exc:
            logger.error(f'Erreur lors de la configuration de l\'entête de cache: {exc}')
        return response

    @app.errorhandler(400)
    def bad_request(e):
        return get_error_response(request, 400, 'bad_request')

    @app.errorhandler(403)
    def forbidden(e):
        return get_error_response(request, 403, 'forbidden')

    @app.errorhandler(404)
    def not_found(e):
        return get_error_response(request, 404, 'not_found')

    @app.errorhandler(500)
    def internal_server_error(e):
        return get_error_response(request, 500, 'internal_server_error')

    @app.errorhandler(429)
    def too_many_requests_error(e):
        return get_error_response(request, 429, 'too_many_requests')

    # On ne garde que les administrations qui ont au moins un RAA
    app.config['AVAILABLE_ADMINISTRATIONS'] = data.get_available_administrations(app)

    # On vérifie que TRUSTED_HOSTS est configuré pour la production
    if not app.config['DEBUG']:
        if not app.config['TRUSTED_HOSTS']:
            logger.error(f'La configuration TRUSTED_HOSTS n\'est pas paramétrée.')
            sys.exit(1)
        if isinstance(app.config['TRUSTED_HOSTS'], str):
            app.config['TRUSTED_HOSTS'] = app.config['TRUSTED_HOSTS'].split(',')
    else:
        app.config['TRUSTED_HOSTS'] = None

    return app

def get_error_response(request, error_code_number, error_code):
    if request.path.startswith('/api/'):
        content = jsonify({'error': error_code})
    else:
        content = render_template(f'{error_code_number}.html')
    response = make_response(content, error_code_number)
    response.headers.set('X-Robots-Tag', 'noindex')
    return response

def config_yaml_loader(file):
    return yaml.load(file, Loader=yaml.FullLoader)
