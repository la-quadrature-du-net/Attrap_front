import re
import datetime
import locale

from flask import abort
from flask import Blueprint
from flask import current_app
from flask import jsonify
from flask import make_response
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for

import pytz
from feedgen.feed import FeedGenerator

from . import cache

bp = Blueprint("search", __name__)


@bp.route('/search', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def search():
    try:
        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    except Exception:
        pass
    search = request.args.get('s', '', type=str)
    administrations = get_selected_administrations(request.args.getlist('administration'))
    start_date = get_date(request.args.get('start_date', '', type=str))
    end_date = get_date(request.args.get('end_date', '', type=str))
    sort = get_sort(request.args.get('sort', '', type=str))

    size = 10
    try:
        size = int(request.args.get('size', '10'))
        if size > 50:
            size = 50
    except:
        size = 10

    page = 1
    try:
        page = int(request.args.get('page', '1'))
        if page <= 1:
            page = 1
    except:
        page = 1

    result=''
    api_url=None
    pager_url=None
    administrations_in_url=None
    size_in_url=None
    page_in_url=None
    start_date_in_url=None
    end_date_in_url=None
    if not search == '':
        result = search_in_opensearch(
            search,
            size=size,
            page=page,
            pre_tags=['<mark>'],
            post_tags=['</mark>'],
            administrations=administrations,
            start_date=start_date,
            end_date=end_date,
            sort=sort
        )

        # On calcule divers variables nécessaires pour le rendu HTML
        if len(administrations) != len(list(current_app.config['AVAILABLE_ADMINISTRATIONS'].keys())):
            administrations_in_url = ','.join(administrations)
        if size != 10:
            size_in_url = size
        if page != 1:
            page_in_url = page
        if start_date and start_date != '':
            start_date_in_url = start_date
        if end_date and end_date != '':
            end_date_in_url = end_date

        # On transforme la date en quelque chose de lisible
        elements = []
        for raa in result['elements']:
            raa['date'] = datetime.datetime.strptime(raa['date'], '%Y-%m-%d').strftime('%d %B %Y')
            elements.append(raa)
        result['elements'] = elements

    return render_template(
        'search/search.html',
        search=search,
        result=result,
        api_url=api_url,
        pager_url=pager_url,
        page=page,
        page_in_url=page_in_url,
        size=size,
        size_in_url=size_in_url,
        administrations=administrations,
        administrations_in_url=administrations_in_url,
        start_date=start_date,
        start_date_in_url=start_date_in_url,
        end_date=end_date,
        end_date_in_url=end_date_in_url,
        sort=sort
    )

@bp.route('/feed/search', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def feed_search():
    search = request.args.get('s', '', type=str)
    administrations=get_selected_administrations(request.args.getlist('administration'))
    start_date = get_date(request.args.get('start_date', '', type=str))
    end_date = get_date(request.args.get('end_date', '', type=str))
    sort = get_sort(request.args.get('sort', '', type=str))
    feed = request.args.get('feed', 'atom', type=str)

    administrations_in_url=None
    if len(administrations) != len(list(current_app.config['AVAILABLE_ADMINISTRATIONS'].keys())):
        administrations_in_url = ','.join(administrations)

    link = url_for('search.search', s=search, administration=administrations_in_url, page=1, start_date=start_date, end_date=end_date, sort=sort, _external=True)

    results = search_in_opensearch(
        search,
        administrations=administrations,
        start_date=start_date,
        end_date=end_date,
        sort=sort,
        pre_tags=['<mark>'],
        post_tags=['</mark>'],
    )

    fg = FeedGenerator()
    fg.id(link)
    fg.title(f'Recherche Attrap {search}')
    fg.subtitle(f'Derniers RAA correspondant à la recherche « {search} »')
    fg.link(href=link)
    fg.language('fr')
    fg.generator(generator='Attrap', uri='https://git.laquadrature.net/la-quadrature-du-net/Attrap_front')

    last_date = None
    for element in results.get('elements', []):
        fe = fg.add_entry()
        link = url_for('raa.raa', raa_id=element['id'], _external=True)
        tz = pytz.timezone(element['timezone'])
        date = datetime.datetime.strptime(element['date'], '%Y-%m-%d').strftime('%d/%m/%Y')
        first_seen_on = datetime.datetime.fromisoformat(element['first_seen_on']).astimezone(tz)

        fe.id(link)
        fe.title(element['name']+' ('+date+')')
        fe.link(href=link)
        fe.content(' '.join(element['content']), type='html')
        fe.author({'name':element['administration_name']})
        fe.pubDate(first_seen_on)
        fe.updated(first_seen_on)
        if not last_date or last_date < first_seen_on:
            last_date = first_seen_on

    fg.lastBuildDate(last_date)

    if not feed == 'rss':
        response = make_response(fg.atom_str(pretty=True))
    else:
        response = make_response(fg.rss_str(pretty=True))
    response.headers.set('Content-Type', 'application/atom+xml; charset=UTF-8')

    return response

@bp.route('/api/v1/search', methods=['GET'])
@cache.cached(timeout=0, query_string=True)
def api_v1_search():
    search = request.args.get('s', '', type=str)
    administrations=get_selected_administrations(request.args.getlist('administration'))
    start_date = get_date(request.args.get('start_date', '', type=str))
    end_date = get_date(request.args.get('end_date', '', type=str))
    sort = get_sort(request.args.get('sort', '', type=str))

    size = 10
    try:
        size = int(request.args.get('size', '10'))
        if size > 50:
            size = 50
    except:
        size = 10

    page = 1
    try:
        page = int(request.args.get('page', '1'))
        if page <= 1:
            page = 1
    except:
        page = 1

    return jsonify(
        search_in_opensearch(
            search,
            size=size,
            page=page,
            administrations=administrations,
            start_date=start_date,
            end_date=end_date,
            sort=sort
        )
    )

def search_in_opensearch(search, size=10, page=1, pre_tags=None, post_tags=None, administrations=None, start_date=None, end_date=None, sort='relevance'):
    if len(administrations) == len(list(current_app.config['AVAILABLE_ADMINISTRATIONS'].keys())):
        administrations = None

    if not search or search == '':
        result = {
            'total': 0,
            'elements': []
        }
        return result
    else:
        query = {
            'from': (page-1)*size,
            'size': size,
            'query': {
                'bool': {
                    'must': [
                        {
                            'query_string': {
                                'fields' : ['content'],
                                'query': f'{search}'
                            }
                        }
                    ]
                }
            },
            'highlight': {
                'fields': {
                    'content': {'type': 'plain'}
                }
            }
        }

        if administrations is not None:
            administrations_filter = {
                'bool': {
                    'should': []
                }
            }
            for administration in administrations:
                administrations_filter['bool']['should'].append(
                    {
                        'term': {
                            'administration': administration
                        }
                    }
                )
            query['query']['bool']['must'].append(administrations_filter)

        if start_date is not None:
            start_date_filter = {
                'range': {
                    'date': {}
                }
            }
            start_date_filter['range']['date']['gte'] = start_date
            query['query']['bool']['must'].append(start_date_filter)

        if end_date is not None:
            end_date_filter = {
                'range': {
                    'date': {}
                }
            }
            end_date_filter['range']['date']['lte'] = end_date
            query['query']['bool']['must'].append(end_date_filter)

        if sort != 'relevance':
            if sort == 'desc' or sort == 'asc':
                query['sort'] = [
                    {
                        'date': {
                            'order': sort,
                        }
                    },
                    {
                        'first_seen_on': {
                            'order': sort,
                        }
                    }
                ]
            if sort == 'first_seen_on_desc' or sort == 'first_seen_on_asc':
                if sort == 'first_seen_on_desc':
                    order = 'desc'
                if sort == 'first_seen_on_asc':
                    order = 'asc'
                query['sort'] = [
                    {
                        'first_seen_on': {
                            'order': order,
                        }
                    }
                ]

        if pre_tags:
            query['highlight']['pre_tags'] = []
            for tag in pre_tags:
                query['highlight']['pre_tags'].append(tag)

        if post_tags:
            query['highlight']['post_tags'] = []
            for tag in post_tags:
                query['highlight']['post_tags'].append(tag)

        response = current_app.opensearch.search(
            body = query,
            index = 'attrap-index'
        )

        result = {
            'total': int(response['hits']['total']['value']),
            'elements': []
        }

        for raa in response['hits']['hits']:
            raa['_source']['content'] = []
            for content in raa['highlight']['content']:
                raa['_source']['content'].append(content.replace("\n", ' '))

            if current_app.config['AVAILABLE_ADMINISTRATIONS'].get(raa['_source']['administration']):
                raa['_source']['administration_name'] = current_app.config['AVAILABLE_ADMINISTRATIONS'].get(raa['_source']['administration'])
            else:
                raa['_source']['administration_name'] = raa['_source']['administration']
            result['elements'].append(raa['_source'] | { 'id': raa['_id'] })

        return result

def get_selected_administrations(args):
    administrations = args
    available_administrations = list(current_app.config['AVAILABLE_ADMINISTRATIONS'].keys())

    if len(administrations) == 1:
        administrations = administrations[0].split(',')
    
    administrations.sort()
    available_administrations.sort()
    administrations = [e for e in administrations if e in available_administrations]
    if not administrations:
        administrations = available_administrations
    return administrations

def get_date(date):
    try:
        if date != '' and date is not None:
            return datetime.datetime.strptime(date, '%Y-%m-%d').strftime('%Y-%m-%d')
        else:
            return None
    except:
        abort(400)

def get_sort(sort):
    if not sort or sort == '':
        return 'relevance'
    if sort != 'desc' and sort != 'asc' and sort != 'relevance' and sort != 'first_seen_on_desc' and sort != 'first_seen_on_asc':
        return 'relevance'
    return sort
