import random

from flask import abort
from flask import Blueprint
from flask import current_app
from flask import make_response
from flask import render_template

from . import cache

bp = Blueprint("index", __name__)


@bp.route("/")
@cache.cached(timeout=0)
def index():
    return render_template("index.html", aliases=list(current_app.config['ALIASES'].keys()))

@bp.route("/mentions-legales")
@cache.cached(timeout=0)
def mentions_legales():
    if current_app.config['MENTIONS_LEGALES'] and not current_app.config['MENTIONS_LEGALES'] == '':
        response = make_response(render_template("mentions-legales.html"))
        response.headers.set('X-Robots-Tag', 'noindex')
        return response
    else:
        abort(404)
