/* Si les administrations sont disponibles, c'est qu'on est sur une page de recherche */
if (available_administrations && administrations) {

  /* On active les menus déroulants */
  const dropdownElementList = document.querySelectorAll('.dropdown-toggle');
  const dropdownList = [...dropdownElementList].map(dropdownToggleEl => new bootstrap.Dropdown(dropdownToggleEl));

  /* On calcule la propriété "tout sélectionné"" */
  var all_is_selected = (available_administrations.length == administrations.length) ? true : false;

  /* En fonction de si tout est sélectionné ou non, on affiche une icone différente */
  if (all_is_selected) {
    document.getElementById("select_all_checkbox").checked = true;
    document.getElementById("select_all_checkbox").indeterminate = false;
  } else if (!all_is_selected && administrations.length > 0) {
    document.getElementById("select_all_checkbox").checked = true;
    document.getElementById("select_all_checkbox").indeterminate = true;
  } else {
    document.getElementById("select_all_checkbox").checked = false;
    document.getElementById("select_all_checkbox").indeterminate = false;
  }

  /* On coche toutes les administrations sélectionnées */
  administrations.forEach((a) => {
  document.getElementById("pref_"+a).checked = true;
  });

  /* On calcule le nombre d'administrations selectionnées */
  var selected_administrations = administrations.length;

  /* Quand on administration est (dé)sélectionnée, on recalcule l'icône du bouton "tout sélectionner" */
  available_administrations.forEach((a) => {
  document.getElementById("pref_"+a).addEventListener("click", (event) => {
    if (document.getElementById("pref_"+a).checked) {
      selected_administrations++;
    } else {
      selected_administrations--;
    }

    if (selected_administrations>0 && selected_administrations<available_administrations.length) {
      document.getElementById("select_all_checkbox").checked = true;
      document.getElementById("select_all_checkbox").indeterminate = true;
    } else if (selected_administrations == 0) {
      document.getElementById("select_all_checkbox").checked = false;
      document.getElementById("select_all_checkbox").indeterminate = false;
    } else {
      document.getElementById("select_all_checkbox").checked = true;
      document.getElementById("select_all_checkbox").indeterminate = false;
    }
  });
  });

  /* Quand "tout (dé)sélectionner" est (dé)coché, on (dé)sélectionne les administrations */
  document.getElementById("select_all_checkbox").addEventListener("change", (event) => {
    event.preventDefault();

    document.getElementById("select_all_checkbox").indeterminate = false;

    if (document.getElementById("select_all_checkbox").checked === false) {
      available_administrations.forEach((a) => {
        document.getElementById("pref_"+a).checked = false;
      });

      document.getElementById("select_all_checkbox").checked = false;
      selected_administrations = 0;
    } else {
      available_administrations.forEach((a) => {
        document.getElementById("pref_"+a).checked = true;
      });

      document.getElementById("select_all_checkbox").checked = true;
      selected_administrations = available_administrations.length;
    }
  });

  document.getElementById("search_form").addEventListener("submit", (event) => {
    event.preventDefault();
    /* Quand le formulaire est envoyé, on supprime les valeurs des préfectures si toutes sont cochées */
    if (selected_administrations == available_administrations.length) {
      available_administrations.forEach((a) => {
          document.getElementById("pref_"+a).checked = false;
      });
    }

    /* On supprime également les valeurs qui ont leurs paramètres par défaut (ça fait des URL plus courtes) */
    if (document.getElementById("start_date").value == '') {
      document.getElementById("start_date").disabled = true;
    }
    if (document.getElementById("end_date").value == '') {
      document.getElementById("end_date").disabled = true;
    }
    if (document.getElementById("sort").value == 'relevance') {
      document.getElementById("sort").disabled = true;
    }

    /* On envoie le formulaire */
    document.getElementById("search_form").submit();
  });
}

/* Bouton de switch sur les pages de RAA */
let documentSwitcher = document.getElementById('document-switcher');

if (documentSwitcher !== null && documentSwitcher) {
  var documentSwitcherStatus = 'txt';
  const documentUrl = document.getElementById('document-url').attributes.href.value;

  var documentTxt = document.getElementById('document-content-txt');
  var documentPdf = document.getElementById('document-content-pdf');
  var switcherText = document.getElementById('switcher-text');

  var administration = document.getElementById('raa-administration').innerText;

  if (administration != "ppparis") {
    documentSwitcher.removeAttribute('href');
    document.getElementById('switcher-icon').innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-left-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5m14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5"/></svg>';
    documentSwitcher.addEventListener("click", (event) => {
      if (documentSwitcherStatus == 'txt') {
        documentTxt.style.display = 'none';
        documentPdf.style.display = 'block';
        let pdf = documentPdf.querySelector('object');
        if (!pdf.data || pdf.data == '') {
          pdf.data = documentUrl;
        }
        documentSwitcherStatus = 'pdf';
        switcherText.innerText = 'Afficher le texte uniquement';
      } else {
        documentTxt.style.display = 'block';
        documentPdf.style.display = 'none';
        documentSwitcherStatus = 'txt';
        switcherText.innerText = 'Afficher le document d’origine';
      }
    });
  }
}
