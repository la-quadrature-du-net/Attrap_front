# Attrap_front

Une [interface web](https://attrap.fr) pour faire des recherches dans les Recueils des actes administratifs (RAA) analysés par [Attrap](https://git.laquadrature.net/la-quadrature-du-net/Attrap).

## Installation

Il est recommandé d'utiliser virtualenv :

```bash
virtualenv --python=/usr/bin/python3 .
source bin/activate
pip3 install -r requirements.txt
```

Vous aurez également besoin d'une instance [OpenSearch](https://opensearch.org/) et Redis ou Valkey.

## Indexation des RAA

Avant la première indexation, vous devez créer l'index dans OpenSeach :

```
flask data create-index
```

Vous devez ensuite copier le dossier `data/` de Attrap dans ce dossier, puis indexer les fichiers :

```
flask data index-files
```

Vous pouvez également récupérer les fichiers depuis le S3 de la CI de Attrap avec la commande :

```
flask data download-files --no-fail
```

## Webhook

Une fois que vous avez paramétré un secret dans `UPDATE_HOOK_SECRET_KEY`, vous pouvez déclencher la mise à jour des données à partir de deux URL :

- Avec une requête GET vers `/data/update-hook?secret_key=<SECRET>&administration=<ADMINISTRATION>`.
- En configurant un webhook dans Gitlab vers `/data/update-hook-gitlab`. Type: `Événements en lien avec les jobs`. L'entête `X-Gitlab-Token` doit être le secret.


## Développement

```bash
flask run --debug
```

Attrap_front sera disponible à l'adresse http://127.0.0.1:5000/

## Docker

### Image tout en un (test uniquement)

Une image Docker avec Opensearch et Redis est disponible pour faire des tests. Il n'est pas recommandé de l'utiliser en production.

```bash
touch ${PWD}/config.yml
docker run -d \
	--name attrap_front \
	--restart always \
	-e GUNICORN_WORKERS=4 \
	-v ${PWD}/data:/opt/Attrap_front/data \
	-v ${PWD}/opensearch-data:/var/lib/opensearch \
	-v ${PWD}/config.yml:/opt/Attrap_front/config.yml:ro \
	-p 127.0.0.1:8001:8000 \
	registry.git.laquadrature.net/la-quadrature-du-net/attrap_front:latest-all-in-one
```

Attrap_front sera disponible à l'adresse http://127.0.0.1:8001/. Vous pouvez ensuite configurer un reverse proxy pour atteindre cette adresse.

### Docker Compose

Vous pouvez utiliser le fichier de configuration dans `misc/docker-compose.yml`. Le fichier de configuration doit préciser les noms d'hôtes Valkey et Opensearch correspondant aux hôtes Docker Compose. Pour lancer Attrap_front avec Docker Compose :

```bash
docker compose up -d
```

Si le conteneur OpenSearch ne se lance pas avec l'erreur `max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]`, lancez la commande suivante :

```bash
sysctl -w vm.max_map_count=262144
```

### Création des données dans Docker

Pour créer l'index, télécharger les fichiers et les indexer :

```bash
docker exec attrap_front flask data create-index
docker exec attrap_front flask data download-files --no-fail
docker exec attrap_front flask data index-files
```

## Mise en production

Vous aurez besoin d'une WSGI et d'un reverse proxy pour mettre en production Attrap_front. Vous pouvez notamment utiliser gunicorn :

```bash
FLASK_CONFIG=production bin/gunicorn -w 4 'app:create_app()'
```

L'application écoutera ensuite sur le port 8000. Vous pouvez ensuite configurer nginx sur ce port.

En créant un fichier `config.yml`, vous pouvez surcharger les variables par défaut de `config.default.yml`. La variable `MENTIONS_LEGALES` devrait au moins être surchargée. Vous devez obligatoirement avoir configuré le nom d'hôte de l'application dans la variable `TRUSTED_HOSTS`.

## Licence

[CeCILL_V2.1-fr](https://cecill.info/licences/Licence_CeCILL_V2.1-fr.html) (voir le fichier `LICENSE`)
